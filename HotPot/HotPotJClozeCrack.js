{
  for (i = 0; i < I.length; i++)
  {
    GAPI = document.getElementById("Gap" + i);
    ANSWER = TrimString(I[i][1][0][0]);
    if ( GAPI.length > 0 && ! CaseSensitive )
    {
      ANSWERU = ANSWER.toUpperCase();
      for (x = 1; x < GAPI.length; x++)
      {
        OP = GAPI.options[x].value;
        OPU = OP.toUpperCase();
        if ( ANSWERU == OPU )
        {
          GAPI.value = OP;
          break;
        }
      }
    }
    else
    {
      GAPI.value = ANSWER;
    }
  }
}
