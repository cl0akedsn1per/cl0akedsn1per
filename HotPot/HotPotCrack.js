{
  var IsJCloze = false;
  var IsJCross = false;
  var IsJMatch = false;
  var IsJMatchDragDrop = false;
  var IsJMix = false;
  var IsJQuiz = false;

  function getMetaContents(mn)
  {
    var m = document.getElementsByTagName("meta");
    for (var i in m)
    {
      if (m[i].name == mn)
      {
        return m[i].content;
      }
    }
    return "";
  }

  function isJCloze()
  {
    if (document.getElementById('Gap0'))
    {
      IsJCloze = true;
      return true;
    }
    else
    {
      IsJCloze = false;
    }
    return false;
  }

  function isJCross()
  {
    if (document.getElementById('L_0_0'))
    {
      IsJCross = true;
      return true;
    }
    else
    {
      IsJCross = false;
    }
    return false;
  }

  function isJMatch()
  {
    if (document.getElementById('s0_0'))
    {
      IsJMatch = true;
      return true;
    }
    else
    {
      IsJMatch = false;
    }
    return false;
  }

  function isJMatchDragDrop()
  {
    if (document.getElementById('D0'))
    {
      IsJMatchDragDrop = true;
      return true;
    }
    else
    {
      IsJMatchDragDrop = false;
    }
    return false;
  }

  function isJMix()
  {
    if (Answers[0])
    {
      IsJMix = true;
      return true;
    }
    else
    {
      IsJMix = false;
    }
    return false;
  }

  function isJQuiz()
  {
    if (document.getElementById('Q_0_0_Btn') || document.getElementById('Q_0_Guess'))
    {
      IsJQuiz = true;
      return true;
    }
    else
    {
      IsJQuiz = false;
    }
    return false;
  }

  function JClozeCrack()
  {
    for (var i = 0; i < I.length; i++)
    {
      GAPI = document.getElementById("Gap" + i);
      ANSWER = TrimString(I[i][1][0][0]);
      if (GAPI.length > 0 && !CaseSensitive)
      {
        ANSWERU = ANSWER.toUpperCase();
        for (x = 1; x < GAPI.length; x++)
        {
          OP = GAPI.options[x].value;
          OPU = OP.toUpperCase();
          if (ANSWERU == OPU)
          {
            GAPI.value = OP;
            break;
          }
        }
      }
      else
      {
        GAPI.value = ANSWER;
      }
    }
  }

  function JCrossCrack()
  {
    for (var i = 0; i < L.length; i++)
    {
      for (var j = 0; j < L[i].length; j++)
      {
        if (L[i][j] !== '')
        {
          EnterAnswer(L[i][j], 1, 1, i, j);
        }
      }
    }
  }

  function JMatchCrack()
  {
    for (var i = 0; i < Status.length; i++)
    {
      Select = document.getElementById(Status[i][2]);
      Key = GetKeyFromSelect(Select);
      Select.value = Key;
    }
  }

  function JMatchDragDropCrack()
  {
      for ( i = 0; i < D.length; i++ ) {
          x = D[i][1];
          var y;
          for ( y = 0; y < F.length; y++ ) {
              if (F[y][1] == x) {
                  break;
              }
          }
          DC[i].DockToR(FC[y]);
          D[i][2] = F[y][1];
          DC[i].tag = y + 1;
      }
  }

  function JQuizCrack()
  {
    ShowHideQuestions();
    for (var i = 0; i < I.length; i++)
    {
      for (var x = 0; x < I[i][3].length; x++)
      {
        if (I[i][3][x][2] > 0)
        {
          theelement = document.getElementById("Q_" + i + "_" + x + "_Btn");
          if (theelement)
          {
            theelement.innerHTML = "CLICK ME";
            if (i < (I.length - 1))
            {
              CheckMCAnswer(i, x, theelement);
            }
          }
          theelement = document.getElementById("Q_" + i + "_Guess");
          if (theelement)
          {
            theelement.value = I[i][3][x][0];
            if (i < (I.length - 1))
            {
              CheckShortAnswer(i);
            }
          }
        }
      }
    }
  }

  function JMixCrack()
  {
    ShowMessage("The answer is:<br /><b>" + CompileString(Answers[0]) + "</b>");
  }

  if (getMetaContents("author").indexOf("Created with Hot Potatoes by Half-Baked Software") != -1)
  {
    if (isJCloze())
    {
      JClozeCrack();
    }
    else if (isJCross())
    {
      JCrossCrack();
    }
    else if (isJMatch())
    {
      JMatchCrack();
    }
    else if (isJMatchDragDrop())
    {
      JMatchDragDropCrack();
    }
    else if (isJQuiz())
    {
      JQuizCrack();
    }
    else if (isJMix())
    {
      JMixCrack();
    }
  }
}


function DroppedOnFixed(DNum) {
    for (y = 0; y < F.length; y++) {
        if (F[y][1] == D[DNum][1]) {
            return y;
        }
    }
    return -1;
}
